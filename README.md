# Sorting script to sort your sequence based on blast results to a given orthogroup set #

### Requirement ###

The script needs a tab delimited list of all sequence names in each orthogroup. See file 22_genomes.ortho.txt for example.

The blast output should be in tabular output format (without headers). Make sure there is only one hit for each sequence.
You need to create a length file for your sequence file. You can create this using the get_seq_len_stats.pl script. This script will create a length stats file. The main sorting script should be called as follows:

`sort_blast_to_orthogroups.pl -b blast_result_file.txt -o file_containing_ortho_group_info.txt -l sequence_length_stats_file.txt -s summary_output_file.txt [-i 0|1 ] [-g Genome_names ]`

Optuions in [] are optional. The option i determines if the sequence names from the orthogroup set should be included in the output.
The option g takes in a comma delimited string of genome names. This option when specified, limits the genomes from which the sequence in the orthogroup file will be included in the sorted output.
