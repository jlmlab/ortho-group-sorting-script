#!/usr/bin/perl -w

use strict;

my $seqfile = $ARGV[0];
my $full_stats = $ARGV[1] || "no";

my ($id,$seq) = ("","");
my @len;
open IN, "< $seqfile";
open OUT, "> $seqfile.len.txt";
while (<IN>){
	chomp;
	if (/>(\S+)/){
		# set the previous info
		unless ($id eq ""){
			my $len = length($seq);
			push (@len,$len) if ($full_stats eq "yes");
			print OUT "$id\t$len\n";;
		}
		$id = $1;
		$seq = "";
	} else {
		$seq .= $_;
	}
}
# deal with the last squence
my $len = length($seq);
push (@len,$len) if ($full_stats eq "yes");
print OUT "$id\t$len\n";
close OUT;

if ($full_stats eq "yes"){
	use Statistics::Descriptive;

	my $stat = Statistics::Descriptive::Full->new();
	$stat->add_data(@len);
	my $seqcount = $stat->count();
	my $mean = sprintf("%.2f", $stat->mean());
	my $median = sprintf("%.2f", $stat->median());
	my $variance = sprintf("%.2f", $stat->variance());
	my $stdev = sprintf("%.2f", $stat->standard_deviation());
	my $min = sprintf("%.2f", $stat->min());
	my $max = sprintf("%.2f", $stat->max());
	my $sum = sprintf("%.0f", $stat->sum());
	my $mb = sprintf("%.2f", $sum/1000000);

	open OUT, ">$seqfile.seq_stats.txt";
	print OUT "seqfile\tseqcount\tmean\tstdev\tmedian\tmin\tmax\tsum\tmb\n";
	print OUT "$seqfile\t$seqcount\t$mean\t$stdev\t$median\t$min\t$max\t$sum\t$mb\n";
	close OUT;
}

exit;
